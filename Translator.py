#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import List, Dict
from pathlib import Path
import json
import re


def tr(source_str: str, local: str = "en"):
    try:
        module = source_str.split(".")[0].replace("#_", "")
        str_id = source_str.split(".")[1].replace("\"", "")
        json_data = load_translation_json("translations/{}.json".format(str(module)))
        # Return translated string or english is both dont exist return original source_str
        translated_str = json_data.get(str_id, {}).get(local)
        if translated_str in ["", None]:
            translated_str = json_data.get(str_id, {}).get("en")
            if translated_str in ["", None]:
                translated_str = source_str
    except:
        translated_str = source_str

    return translated_str


def load_translation_json(file_name):
    try:
        with open(file_name, encoding="utf-8") as f:
            json_data = json.load(f)
    except (FileNotFoundError, json.JSONDecodeError):
        json_data = {}
    return json_data


def save_translation_json(file_name, json_data):
    with open(file_name, 'w', encoding="utf-8") as outfile:
        json.dump(json_data, outfile, sort_keys=True, indent=4, separators=(',', ': '))


def generate(source_path: str, ignore_list: List[str] = []):
    output_path = "translations"
    out_path = Path(output_path)
    out_path.mkdir(parents=True, exist_ok=True)
    path = Path(source_path)
    for python_file in path.glob('**/*.py'):
        if not any(ignore_dir == str(python_file)[0:len(ignore_dir)] for ignore_dir in ignore_list):
            update_translations(python_file)


def update_translations(python_file: Path):
    regex_translation = re.compile(r'\"#_.*\..*\"')
    regex_context = re.compile(r'\/\*.*\*\/')

    with python_file.open() as f:
        lines = f.readlines()
        previous_line = ""
        for line in lines:
            matches = regex_translation.findall(line)
            if len(regex_context.findall(previous_line)) > 0:
                context = previous_line.replace("# ", "").replace("/*", "").replace("*/", "")
                context = context.strip()
            else:
                context = None
            for match_str in matches:
                match_str = match_str.replace("\"", "")
                str_id = match_str.split(".")[1]
                module = match_str.split(".")[0].replace("#_", "")
                filename = "translations/{}.json".format(module)
                json_data = load_translation_json(filename)
                if json_data.get(str_id) is None:
                    json_data[str_id] = {"en": "", "fr": "", "es": "", "it": ""}
                if context is not None:
                    json_data[str_id]["context"] = context
                if len(json_data.keys()) > 0:
                    save_translation_json(filename, json_data)
            previous_line = line

